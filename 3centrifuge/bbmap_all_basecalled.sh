#!/bin/bash
#SBATCH --job-name=bbmap_filter_reads
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
module load bbmap

filterbyname.sh in=/core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/guppy_basecalled_readsBA/all_basecalled_readsBA.fasta out=all_basecalled_readsBA_uncontaminated.fastq names=nodups_contam_read_ids_basecalledBA.txt

filterbyname.sh in=/core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/guppy_basecalled_readsWA/nodups_all_basecalled_readsWA.fasta out=all_basecalled_readsWA_uncontaminated.fastq names=nodups_contam_read_ids_basecalledWA.txt

filterbyname.sh in=/core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/guppy_basecalled_readsPA/all_guppy_basecalled_reads_passPA.fasta out=all_basecalled_readsPA_uncontaminated.fastq  names=nodups_contam_read_ids_basecalledPA.txt
