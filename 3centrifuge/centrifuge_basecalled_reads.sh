#!/bin/bash
#SBATCH --job-name=centrifuge
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH -p general
#SBATCH -q general
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -o %x_%j.err

echo 'hostname'
module load centrifuge

centrifuge -f \
        -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v \
        -p 12 \
        --report-file reportWA.tsv \
        --quiet \
        --min-hitlen 50 \
        -U /core/labs/Wegrzyn/PumpkinAshGenome/basecall/guppy_basecalled_readsWA/nodups_all_basecalled_readsWA.fasta \
        >classificationsWA_basecalled.txt

centrifuge -f \
        -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v \
        -p 12 \
        --report-file reportPA.tsv \
        --quiet \
        --min-hitlen 50 \
        -U /core/labs/Wegrzyn/PumpkinAshGenome/basecall/guppy_basecalled_readsPA/all_guppy_basecalled_reads_passPA.fasta \
        >classificationsPA_basecalled.txt

centrifuge -f \
        -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v \
        -p 12 \
        --report-file reportBA.tsv \
        --quiet \
        --min-hitlen 50 \
        -U /core/labs/Wegrzyn/PumpkinAshGenome/basecall/guppy_basecalled_readsBA/all_basecalled_readsBA.fasta \
        >classificationsBA_basecalled.txt


