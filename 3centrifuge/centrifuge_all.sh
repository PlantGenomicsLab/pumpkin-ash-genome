#!/bin/bash
#SBATCH --job-name=centrifuge_all
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 10
#SBATCH -p general
#SBATCH -q general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -o %x_%j.err

echo 'hostname'
module load centrifuge

centrifuge -f \
        -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v \
        -p 12 \
        --report-file reportBA.tsv \
        --quiet \
        --min-hitlen 50 \
        -U ../../blackash/sratools/ERR6641555.fasta \
        >classificationsBA.txt

centrifuge -f \
        -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v \
        -p 12 \
        --report-file reportBA.tsv \
        --quiet \
        --min-hitlen 50 \
        -U ../../blackash/sratools/ERR6641554.fasta \
        >classificationsBA.txt

centrifuge -f \
        -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v \
        -p 12 \
        --report-file reportBA.tsv \
        --quiet \
        --min-hitlen 50 \
        -U ../../blackash/sratools/ERR6641553.fasta \
        >classificationsBA.txt

centrifuge -f \
        -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v \
        -p 12 \
        --report-file reportWA.tsv \
        --quiet \
        --min-hitlen 50 \
        -U ../../whiteash/sratools/ERR6641552.fasta \
        >classificationsWA.txt

centrifuge -f \
        -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v \
        -p 12 \
        --report-file reportWA.tsv \
        --quiet \
        --min-hitlen 50 \
        -U ../../whiteash/sratools/ERR6641551.fasta \
        >classificationsWA.txt

centrifuge -f \
        -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v \
        -p 12 \
        --report-file reportWA.tsv \
        --quiet \
        --min-hitlen 50 \
        -U ../../whiteash/sratools/ERR6641550.fasta \
        >classificationsWA.txt

centrifuge -f \
        -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v \
        -p 12 \
        --report-file report.tsv \
        --quiet \
        --min-hitlen 50 \
        -U ../data/ERR6641559.fasta \
        >classifications.txt

centrifuge -f \
        -x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v \
        -p 12 \
        --report-file report.tsv \
        --quiet \
        --min-hitlen 50 \
        -U ../data/ERR6641560.fasta \
        >classifications.txt
