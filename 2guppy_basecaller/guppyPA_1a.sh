#!/bin/bash
#SBATCH --job-name=guppyPA
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 36
#SBATCH --mem=120G
#SBATCH --partition=gpu
#SBATCH --qos=general
#SBATCH -w xanadu-01
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
module load guppy/6.1.2-GPU

guppy_basecaller -c dna_r9.4.1_450bps_sup.cfg -i /core/labs/Wegrzyn/PumpkinAshGenome/pumpkinash/fast5/Profunda_1a_07_12_21/20210712_1459_X3_FAQ51478_340e328b/fast5_pass -s /core/labs/Wegrzyn/PumpkinAshGenome/pumpkinash/guppy_basecalled_reads -x cuda:all --num_callers 30 
