#!/bin/bash
#SBATCH -J medaka_basecalledPA
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH -p xeon
#SBATCH -q general
#SBATCH --mem=250G
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module unload python
module load medaka/1.7.1

NPROC=32
BASECALLS=/core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/guppy_basecalled_readsPA/all_guppy_basecalled_reads_passPA.fastq                              
DRAFT=/core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/flye_assembly/outputPA/assembly.fasta
OUTDIR=/core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/medaka/medakaPA

medaka_consensus -i ${BASECALLS} -d ${DRAFT} -o ${OUTDIR}  -t 32
