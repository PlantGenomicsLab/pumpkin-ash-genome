# Pumpkin Ash Genome Assembly

# Table of Contents
[[_TOC_]]
# Genome Size Estimation
We are able to estimate the genome size of the pumpkin ash genome using the raw reads downloaded from NCBI. Using kmerfreq and GCE softwares, we estimated the genome size for pumpkin ash to be 832 Mb. 

# Nanoplot 
This tool helps to visualize raw read data and metrics for assessing the quality of an assembly. It produces graphs that represent raw read lengths, as well as various sequencing summaries. I ran the program on the raw fastq reads (pass and fail) and the filtered reads after Centrifuge. 

## **Filtered Reads summary**

General summary:         

Mean read length:                 13,237.6

Median read length:               11,101.0

Number of reads:               6,144,503.0

Read length N50:                  21,611.0

STDEV read length:                11,121.0

Total bases:              81,338,536,257.0

![HistogramReadlength](/uploads/4ac58000225bc3d2b1e996cf16825298/HistogramReadlength.png)
## **Raw reads pass**
General summary:         

Mean read length:                 13,233.7

Mean read quality:                    16.2

Median read length:               11,095.0

Median read quality:                  16.5

Number of reads:               6,146,457.0

Read length N50:                  21,611.0

STDEV read length:                11,121.4

Total bases:              81,340,416,972.0

![HistogramReadlength_pass](/uploads/0ac82c7cf6b6c0865e8611be117405ba/HistogramReadlength_pass.png)

## **Raw reads fail** 
General summary:         
Mean read length:                 10,781.1

Mean read quality:                     6.2

Median read length:                6,785.0

Median read quality:                   6.0

Number of reads:               1,892,872.0

Read length N50:                  20,562.0

STDEV read length:                12,803.0

Total bases:              20,407,183,480.0

![HistogramReadlength_fail](/uploads/2228135be0558ef19e24e08d5cc38050/HistogramReadlength_fail.png)

# Genome Assembly
## **1a. Downloading sequence data (Flye)**
The data used to assemble pumpkin, black, and white ash were downloaded from the public database [NCBI Sequence Read Archives](https://www.ncbi.nlm.nih.gov/sra) using SRA Toolkit. Each species had multiple Oxford Nanopore long reads under various accession numbers.

In [the script](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/1sra_toolkit/sratoolkitPA.sh), the corresponding accession numbers for each species were used (pumpkin, white, and black ash were downloaded in separate scripts, but you could combine them into one).

## **1b. Downloading sequence data (Long Read Re-basecalling)**
Basecalling translates electrical signals through a nanopore into nucleotide sequences. I used the Guppy program to re-basecall the long read sequences found in SRA.

[Guppy script](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/2guppy_basecaller/guppyPA_1a.sh)

*Input:* fast5 file reads (downloaded from SRA using `wget` and the link to the fast5 files)

*Output:* folder with fastq files of basecalled reads that can be concatenated

**[Parameters](https://denbi-nanopore-training-course.readthedocs.io/en/latest/basecalling/basecalling.html)**
- -c     config file (or --flowcell and --kit for names of flowcell and kit used)      
       - if you know the flowcell and kit name you can look up the config file: `module display guppy` to get the path, `module load guppy/5.0.16-CPU`, `guppy_basecaller --print_workflows` to load all the possible flowcell + kit and the corresponding config file if needed
 - -i     input in fast5 format
 - -s     save file
 - -x cuda_all   use all allocated GPUs
 - -num_callers  numbers of parallel basecallers to use 
 - Guppy requires you to be on the gpu partition, meaning that only certain nodes can be requested. [This page](https://bioinformatics.uconn.edu/resources-and-events/tutorials-2/xanadu/) can help distinguish the different types of partitions and you can use `sinfo -N -l -p gpu` to find which nodes are specific for gpu. I added `#SBATCH -w xanadu-01` to specify which gpu node to use. 
- The output of Guppy produces a few folders. The .fastq files can be found in the "pass" folder and using the `cat` command you can concatenate them into one large .fastq file. 


## **2. Filtering out contaminants**
I used the Centrifuge program to classify sequences of different taxons and identify contaminated reads within the sequence data. This allowed me to remove filter out reads that matched >40% of query length of another taxon using the bbmap program (as classified by Centrifuge).

Centrifuge input requires fasta format but SRA Toolkit output was in fastq.gz file format, so I used 
`seqtk seq -a input.fastq.gz > output.fasta` 
to convert each output to fasta format.

#### **a. Run the [Centrifuge script](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/tree/main/**3centrifuge) on your downloaded reads to classify sequences**


#### **b. Centrifuge output analysis**

- To find average hit length: `grep -v "unclassified" classifications.txt | awk '{FS = "\t"}{x+=$6}END{print x/NR}'`
- Add a column for hits that matched >40% of the query sequence: `tail -n +2 classifications.txt | awk '{FS = "\t"}{OFS="\t"}{x=$6/$7}{print $0,x}' | awk '{FS = "\t"}{if ($9 > 0.4) print $0}' > contam_reads_40%.txt`         
- Count number of  classified sequences that match >40% of query sequence using `wc -l`. You can also use `awk '{s++}END{print s/4}'` to compare this number to the total number of reads overall.

- Make new file containing only the IDs of those contaminants: `awk NF=1 contam_reads_40% > contam_reads_40%_ids.txt`
- Remove duplicate IDs: `sort -u contam_reads_40%_ids.txt > nodups_contam_reads_40%.txt`

#### **c. Remove classified sequences using bbmap**
Using the new file containing the contaminated reads, [this bbmap script](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/3centrifuge/bbmap_all_basecalled.sh) will remove those reads from the inital sequences.  

### *General process:*
- convert downloaded sequence read files from .fastq.gz to .fasta format
- run Centrifuge to identify sequences that match >40% of a classified taxon (output is a list of IDs corresponding to contaminated sequences)
- remove those sequences from total reads using bbmap (requires original .fastq file and the Centrifuge output)

(Since there were 2 different long reads for pumpkin ash, both reads were filtered separately before assembly)

## **Coverage (after centrifuge Flye)**
Coverage = (Total # of reads x read length)/Genome size

 *     Pumpkin Ash:
        Total length = 8443306523 + 1461256552
        Estimated genome size = 800000000
        Coverage = ~12.38

 *     White Ash:
        Total length = 7885600857 + 4181284138 + 9774861314
        Estimated genome size = 642567000
        Coverage = ~33.99

 *     Black Ash:
        Total length = 6876405050 + 3526399293 + 8136322281
        Estimated genome size = 616419000
        Coverage = ~30.08


## **3. Long read genome assembly with Flye**
I used Flye (de novo assembler that works well with Oxford Nanopore Reads) to assemble the filtered long read data for each species into its respective de novo genome assembly. 

[Flye script](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/4flye_assembler/flye_basecalled_uncontamPA.sh)


### **Completeness and quality assessment**
QUAST and BUSCO were run to assess each Flye de novo genome. Specific results can be found in [this spreadsheet](https://docs.google.com/spreadsheets/d/1qKYV1qkIiihK8lgWq2s-AJxy9RBqvQNJPuFp9FMZgN8/edit#gid=2147242283). 

**BUSCO** (Benchmarking Universal Single-Copy Orthologs) is used to quantitatively assess the quality of an assembly by providing the number of complete BUSCO groups present in the assembly.

<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=buscoPA
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --mem=30G
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "hostname"
module load busco/5.0.0

export PATH=/home/FCAM/estrickland/augustus/bin:$PATH
export PATH=/home/FCAM/estrickland/augustus/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/config/

busco -i ../../pumpkinash/flye/assembly.fasta \
	-o buscoPA -m genome -c 8 -l /isg/shared/databases/BUSCO/odb10/lineages/eukaryota_odb10 
</pre>

**Parameters**
- set the PATH variable
-  **-o** output folder
- **-m** mode (genome, transcriptome, or proteins)
- **-l** lineage dataset

**QUAST** provides statistical data to describe assembly metrics. 
<pre style="color: silver; background: black;">
#!/bin/bash
#SBATCH --job-name=quastPA
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=20G
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo "hostname"

module load quast

quast.py ../../pumpkinash/flye/assembly.fasta 
-o quastPA
</pre>

These scripts are used throughout the assembly and annotation process in order to track the completeness and quality of the assembly. 

## **5. Polishing the Flye assemblies**
After the initial assembly, we use Medaka to polish the assembly. Running the program on the Flye assemblies improves errors and inconsistencies in the draft genome. It produces a cleaner consensus.fasta file that can now be used.

[Medaka script](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/5medaka/medaka_basecalled_assemblyPA.sh) 

I used bbmap to remove contigs <1000 bp from the polished pumpkin ash assembly, and was able to determine the size of the pumpkin ash assembly to be 947527745.

### **Completeness and quality of polished assembly**
Quast and  BUSCO were run again on the polished Flye assembly to check for improvements in completeness and quality. Previous scripts were edited to match the path and parameters for the assembly. Complete data can be found in [this spreadsheet](https://docs.google.com/spreadsheets/d/1qKYV1qkIiihK8lgWq2s-AJxy9RBqvQNJPuFp9FMZgN8/edit?usp=sharing).

BUSCO was run 3 times using different lineage datasets for each assembly: Eukaryote (eukaryota_odb10), green plants (viridiplantae_odb10), seed plants (embryophyta_obd10). The embryophyta database was the best database to use moving foward. 

| Green plants | Complete BUSCOs | Seed plants | Complete BUSCOs |
| ------ | ------ | ------ | ------ |
| Pumpkin | C:99.1%[S:80.0%,D:19.1%],F:0.7%,M:0.2%,n:425 |  | C:95.9%[S:75.8%,D:20.1%],F:2.2%,M:1.9%,n:1614 |
| White | C:98.3%[S:71.5%,D:26.8%],F:1.4%,M:0.3%,n:425 |  | C:95.4%[S:71.3%,D:24.1%],F:2.9%,M:1.7%,n:1614 |
| Black | C:95.0%[S:80.9%,D:14.1%],F:3.3%,M:1.7%,n:425 |  | C:93.7%[S:80.2%,D:13.5%],F:3.0%,M:3.3%,n:1614 |

## **6. Purge Haplotigs**
Purge haplotigs is a program that identifies allelic variants in an assembly and reduces regional duplication of allelic contigs in haploid assemblies. 
### **Step 1: Align sequence data to draft genome sequences**
Use [minimap](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/6purge_scripts/minimap_contigsrmv_rebasecalled_assembly.sh) 
- The input is the filtered reads after running Centrifuge and bbmap.
- The output is a .sam file that needs to be converted to .bam file for Step 2. This requires running Samtools script on the alignment produced: [convert .sam to .bam file](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/6purge_scripts/samtobam_all_basecalled.sh), [sort the file](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/6purge_scripts/samtools_sort_all_basecalled.sh), [index the file](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/6purge_scripts/samtools_index_all_basecalled.sh).

### **Step 2: Read histogram**
Use [readhist script](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/6purge_scripts/readhist_basecalled_aligned.sh) to create a bimodal coverage histogram to be used in Step 3.

#### **Histogram results**
>Rebasecalled Black Ash
![aligned_rebasecall_contigsrmvBA_sorted.bam.histogram](/uploads/f13b526b7b0ce1c299948f53ad118cea/aligned_rebasecall_contigsrmvBA_sorted.bam.histogram.png)

>Rebasecalled Pumpkin Ash
![aligned_rebasecall_contigsrmvPA_sorted.bam.histogram](/uploads/f34e33e8964cce82c60b7612bac25205/aligned_rebasecall_contigsrmvPA_sorted.bam.histogram.png)

>Rebasecalled White Ash
![aligned_rebasecall_contigsrmvWA_sorted.bam.histogram](/uploads/3fb0af2de99e1fcf6e78def160337370/aligned_rebasecall_contigsrmvWA_sorted.bam.histogram.png)

>Flye Black Ash
![polished_flye_alignedBA_contigsrmv_sorted.bam.histogram](/uploads/f6b62aba762a08475409653fcd46bdf2/polished_flye_alignedBA_contigsrmv_sorted.bam.histogram.png)

>Flye Pumpkin Ash
![polished_flye_alignedPA_contigsrmv_sorted.bam.histogram](/uploads/beddf0f0e342bea8f810137fe9cdbf05/polished_flye_alignedPA_contigsrmv_sorted.bam.histogram.png)

>Flye White Ash
![polished_flye_alignedWA_contigsrmv_sorted.bam.histogram](/uploads/22efc5e478fc24507007256193fc940b/polished_flye_alignedWA_contigsrmv_sorted.bam.histogram.png)


### **Step 3: Analyze the coverage**
- This step produces a statistical report of each contigs from the histograms.
- [The script](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/6purge_scripts/contigcov_rebasecall_aligned.sh) utilizes these parameters and values from the histogram
       -i / -in        The bedtools .gencov output that was produced from 'purge_haplotigs readhist'
       -l / -low       The read depth low cutoff (use the histogram to eyeball these cutoffs)
       -h / -high      The read depth high cutoff
       -m / -mid       The low point between the haploid and diploid peaks


### **Step 4: Run the [purge pipeline](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/6purge_scripts/purgePA_basecalled.sh)** 
The last step of purge which reassigns/removes contigs from assembly.

## **Quast and Busco Comparisons**
| | Flye Assembly | Re-Basecalled Assembly |
| ------ | ------ | ------ |
| Coverage after centrifuge | **P:** 12.07 **W:** 25.85 **B:** 25.35 | **P:** 8.19 **W:** 24.798 **B:** 22.739 |
| Coverage after purge | **P:** 12.07 **W:** 25.85 **B:** 25.35 | **P:** 9.11 **W:** 19.95 **B:** 19.14

### *BUSCO after Flye assembly*
| | Flye Assembly | Re-Basecalled Assembly |
| ------ | ------ | ------ |
| Pumpkin | C:96.2%[S:75.1%,D:21.1%],F:2.5%,M:1.3%,n:1614 | C:95.0%[S:76.4%,D:18.6%],F:3.2%,M:1.8%,n:1614 | C:97.6%[S:73.5%,D:24.1%],F:1.6%,M:0.8%,n:1614 |
| White |  C:97.0%[S:65.8%,D:31.2%],F:1.8%,M:1.2%,n:1614 | 
| Black | C:C:95.6%[S:80.6%,D:15.0%],F:2.4%,M:2.0%,n:1614 | C:96.9%[S:79.6%,D:17.3%],F:2.0%,M:1.1%,n:1614 |

### *BUSCO after medaka*
| | Flye Assembly | Re-Basecalled Assembly |
| ------ | ------ | ------ |
| Pumpkin | C:95.9%[S:75.8%,D:20.1%],F:2.2%,M:1.9%,n:1614 | C:94.1%[S:75.8%,D:18.3%],F:3.9%,M:2.0%,n:1614 |
| White | C:C:95.4%[S:71.3%,D:24.1%],F:2.9%,M:1.7%,n:1614 | C:97.7%[S:73.0%,D:24.7%],F:1.6%,M:0.7%,n:1614 |C:96.6%[S:78.9%,D:17.7%],F:2.1%,M:1.3%,n:1614 |
| Black | C:93.7%[S:80.2%,D:13.5%],F:3.0%,M:3.3%,n:1614 | C:96.6%[S:78.9%,D:17.7%],F:2.1%,M:1.3%,n:1614 |

### *BUSCO after Purge*
| | Flye Assembly | Re-Basecalled Assembly |
| ------ | ------ | ------ |
| Pumpkin | C:86.1%[S:77.5%,D:8.6%],F:5.1%,M:8.8%,n:1614 **W:** C:96.5%[S:75.4%,D:21.1%],F:1.9%,M:1.6%,n:1614 | C:93.9%[S:78.3%,D:15.6%],F:4.0%,M:2.1%,n:1614 | 
| White | C:96.5%[S:75.4%,D:21.1%],F:1.9%,M:1.6%,n:1614 | C:97.2%[S:79.9%,D:17.3%],F:1.9%,M:0.9%,n:1614 |
| Black | C:95.4%[S:81.1%,D:14.3%],F:2.4%,M:2.2%,n:1614 | C:96.6%[S:79.7%,D:16.9%],F:2.1%,M:1.3%,n:1614 | 

## **Scaffolding to green ash**
The ordering and orientation of our contiguos read sequences to the published green ash genome and filling in gaps in our assemblies to obtain chromosome scale reference genomes. Essentially, by aligning our assemblies to the published green ash genome [(Huff et al. 2021)](https://onlinelibrary.wiley.com/doi/full/10.1111/1755-0998.13545), RagTag will break and reassemble our ash assemblies based on the order and orientation of the green ash genome.  RagTag functions in multiple steps (after downloading the reference genome)
 
### **Downloading the Green ash genome (previously assembled)**

Assembly data for the green ash genome was found in the NCBI Genome database: https://www.ncbi.nlm.nih.gov/data-hub/genome/GCA_912172775.1/

`curl -OJX GET "https://api.ncbi.nlm.nih.gov/datasets/v1/genome/accession/GCA_912172775.1/download?include_annotation_type=GENOME_GFF,RNA_FASTA,CDS_FASTA,PROT_FASTA&filename=GCA_912172775.1.zip" -H "Accept: application/zip"`

Files downloaded were in the form of separate chromosomes: use `cat` command to concatenate the files

### **Scaffolding steps**
### 1. [Correct](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/7RagTag/ragtag_correct.sh)
Breaking and reordering contigs that are chimeric or do not fully align to the reference.
### 2. [Scaffold](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/7RagTag/scaffold_only.sh)
Pairwise whole genome sequence homology to generate filtered and merged whole genome alignments informed by the reference.
- Black ash: 15249 contigs after scaffolding vs 3696 after final assembly ( BUSCO C:96.8%[S:80.2%,D:16.6%],F:1.9%,M:1.3%,n:1614)
- Pumpkin:  5642 contigs after scaffolding vs 4867 after final assembly (BUSCO C:94.7%[S:79.1%,D:15.6%],F:3.3%,M:2.0%,n:1614)
- White: 6538 contigs after scaffolding vs 5288 after final assembly (BUSCO C:97.4%[S:80.9%,D:16.5%],F:1.7%,M:0.9%,n:1614)

 I decided to stop here as there was only one query assembly for each species. Running just the scaffolding script using green ash and the genome assembly after purge produced better BUSCO and Quast scores. The correcting step was not necessary.  

 ### Determine the longest contigs
 samtools faidx sample.fa
 cut -f1-2 sample.fasta.fai
 Create a bash script to run seqkit: seqkit seq -m 23952309 ragtag.scaffold.fasta > 23longest_contigsPA.fasta
- The m flag is the genome size of the smallest chromosome level contig. For chromosome level there should be a distinct difference in size of contigs in your list.

After scaffolding only, I had a break after 23 contigs, with the smallest contig being 23952309 bp for pumpkin ash (chromosome level assembly).

*Results after scaffolding only*

| N50 | Pumpkin Ash | Black Ash | White Ash |
| ------ | ------ | ------ | ------ |
| After asssembly | 269013 | 442584 | 343760 | 
| After purge | 304590 | 462427 | 416452 |
| After scaffolding | 33726661 | 31675329 | 35008979 |

**BUSCO and Quast**
| Results | Pumpkin Ash | Black Ash | White Ash |
| ----- | ------ | ------ | ----- | 
| BUSCO | C:94.7%[S:79.1%,D:15.6%],F:3.3%,M:2.0%,n:1614 | C:96.8%[S:80.2%,D:16.6%],F:1.9%,M:1.3%,n:1614 | C:97.4%[S:80.9%,D:16.5%],F:1.7%,M:0.9%,n:1614 | 
| Total contigs after scaffolding | 5642 | 15249 | 6538 |
| Min. contig length of 23 chromosome level contigs| 23952309 | 21292712 | 22820373 |

### 3. [Patch](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/7RagTag/ragtag_patch.sh)
Uses one genome assembly to make scaffolding joins between contigs and fill gaps in a second genome assembly, useful for multiple assemblies using different sequencers or assemblers. 
### 4. [Merge](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/7RagTag/ragtag_merge.sh)
Reconciles multiple scaffolds for a given assembly and combines them into a single scaffold. 


# **Genome Annotation of Pumpkin Ash**
 Structural annotation of the genome involves identification of the boundaries of protein coding genes in the genome reference  sequence, also referred to as gene prediction. RNA-seq data can be used as evidence for gene prediction. By building a transcriptome library from RNA-seq reads, they can be aligned to the genome and genes can be predicted. White, black, and blue ash have existing RNA-Seq datasets available in NCBI and will be used as inputs for annotation of pumpkin, black, and white ash genome assemblies.

## **1. Use [sratools](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/blob/main/1sra_toolkit/sratoolkit_rnaseq_libraries_whiteash.sh) to download RNA-seq libraries** 
Data were found on the NCBI database. Black and blue ash only had 1 library each, while white ash had 44 different libraries. All were from leaf tissue (it can be helpful use different tissue types, as the mapping rates can vary. In our case, only leaf tissue was available.)

## **2. Filter the reads**
[Fastp](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/tree/main/8fastp) is a software used to remove the adaptor sequences from the downloaded RNA-seq reads. It also trims any low quality sequences present in the data so as to ensure higher quality reads before alignment and annotation. The filtered and trimmed reads can now be used for alignment to the genome. 

The output of fastp trimming is a a _1.fastq.gz and a _2.fastq.gz file for each library.

## **3. Aligning the reads to the genome**
Read output from fastp was inputed into the [Hisat2 pipeline](https://gitlab.com/PlantGenomicsLab/pumpkin-ash-genome/-/tree/main/9hisat2). This program creates a genome index and maps the RNA-seq libraries to the indexed genome. Using samtools, we also converted the alignment output .sam file to a .bam file that can be more easily used for downstream analysis.

### **Mapping Rate**
Mapping rates for each alignment can be found in the .err files of the script submission. After hisat2 was run on all 46 libraries, we identified the mapping rates for each and established a cutoff rate of 78% (typically above 80% is good, however reducing the cutoff allowed us to include more libraries). There were 6 white ash RNA-seq libraries that mapped greater than 78% to the pumpkin ash genome and were selected to be used for structural annotation with the BRAKER2 annotation program. 

## **4. Structural annotation with BRAKER2**
 The BRAKER2 pipeline predicts genes with extrinsic RNA-seq reads as evidence. The pre-aligned RNA reads are can be used as evidence to predict exon-intron boundaries by BRAKER2. 

 45236 genes were identified in the pumpkin ash assembly.

## **Helpful resources**
https://www.ncbi.nlm.nih.gov/grc/help/definitions/ 
