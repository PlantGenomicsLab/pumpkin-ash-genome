#!/bin/bash
#SBATCH --job-name=fastqer_dump_xanadu
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load sratoolkit/2.11.3

OUTDIR=/core/labs/Wegrzyn/PumpkinAshGenome/rnaseq_data_ash/whiteash

cd ${OUTDIR}

fasterq-dump SRR11744254
fasterq-dump SRR11731569
