#!/bin/bash
#SBATCH -J SRAblackash
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 1
#SBATCH -p general
#SBATCH -q general
#SBATCH --mem=100G
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo 'hostname'
module load sratoolkit

cd /core/labs/Wegrzyn/PumpkinAshGenome/scripts/sra
fastq-dump --gzip ERR6641555
fastq-dump --gzip ERR6641554
fastq-dump --gzip ERR6641553

