#!/bin/bash
#SBATCH --job-name=flyePA_uncontam_rebasecall
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=250G
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo 'hostname'
module load flye

flye --nano-raw /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/bbmap/all_basecalled_readsPA_uncontaminated.fastq \
        --genome-size 7170623747 \
        --threads 16 \
        --out-dir /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/flye_assembly/output
