#!/bin/bash
#SBATCH --job-name=repeatmaskerPA
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=first.last@uconn.edu
#SBATCH --mem=30G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
echo `date`

# load software
module load RepeatMasker/4.1.2

# set variables
REPLIB=/core/labs/Wegrzyn/PumpkinAshGenome/repeatmodeler/pumpkinash/RM_2396045.ThuJan51127352023/consensi.fa
OUTDIR=/core/labs/Wegrzyn/PumpkinAshGenome/repeatmasker/pumpkinash_masked
    mkdir -p ${OUTDIR}
    cd ${OUTDIR}

GENOME=/core/labs/Wegrzyn/PumpkinAshGenome/ragtag_scaffolding/scaffolding_only/scaffold_afterpurgePA/23longest_contigsPA.fasta

RepeatMasker -dir repeatmasker_out -pa 8 -lib ${REPLIB} -gff -a -noisy -xsmall ${GENOME}

