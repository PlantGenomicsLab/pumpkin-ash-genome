#!/bin/bash
#SBATCH --job-name=alignPA
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 10
#SBATCH --mem=50G
#SBATCH --partition=xeon
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err
#SBATCH --array=[1-46]%10

echo `hostname`
module load hisat2/2.2.1
module load samtools/1.12

INDIR=/core/labs/Wegrzyn/PumpkinAshGenome/rnaseq_data_ash/all_trimmed_sequences
OUTDIR=alignmentsPA
mkdir -p $OUTDIR

INDEX=/core/labs/Wegrzyn/PumpkinAshGenome/hisat2/index_outputPA
ACCLIST=/core/labs/Wegrzyn/PumpkinAshGenome/rnaseq_data_ash/accessionlist_all.txt
NUM=$(expr ${SLURM_ARRAY_TASK_ID} + 1)
SAMPLE=$(sed -n ${NUM}p $ACCLIST)

hisat2 \
	-p 2 \
	-x $INDEX \
	-1 $INDIR/${SAMPLE}_trim_1.fastq.gz \
	-2 $INDIR/${SAMPLE}_trim_2.fastq.gz | \
samtools view -@ 1 -S -h -u - | \
samtools sort -@ 1 -T $SAMPLE - >$OUTDIR/$SAMPLE.bam
samtools index $OUTDIR/$SAMPLE.bam


