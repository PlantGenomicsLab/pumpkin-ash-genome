#!/bin/bash
#SBATCH --job-name=hisat2_indexPA
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
echo `date`

module load hisat2/2.2.0

OUTDIR=/core/labs/Wegrzyn/PumpkinAshGenome/hisat2/index_outputPA
mkdir -p $OUTDIR

GENOME=/core/labs/Wegrzyn/PumpkinAshGenome/ragtag_scaffolding/scaffolding_only/scaffold_afterpurgePA/23longest_contigsPA.fasta

hisat2-build -p 16 $GENOME $OUTDIR/pumpkin

