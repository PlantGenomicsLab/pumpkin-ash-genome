#!/bin/bash
#SBATCH -J ragtag_patchBA
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH -p general
#SBATCH -q general
#SBATCH --mem=100G
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
module load RagTag/2.1.0
module load MUMmer/4.0.2

ragtag.py patch /core/labs/Wegrzyn/PumpkinAshGenome/ragtag_scaffolding/ragtag_scaffolded_outputBA/ragtag.scaffold.fasta /core/labs/Wegrzyn/PumpkinAshGenome/greenash_genome/green_ash_genome.fasta -o /core/labs/Wegrzyn/PumpkinAshGenome/ragtag_scaffolding/ragtag_patch_outputBA
