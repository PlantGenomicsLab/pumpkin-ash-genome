#!/bin/bash
#SBATCH -J ragtag_scaffoldpurgeWA
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH -p general
#SBATCH -q general
#SBATCH --mem=100G
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
module load RagTag/2.1.0

ragtag.py scaffold /core/labs/Wegrzyn/PumpkinAshGenome/greenash_genome/green_ash_genome.fasta /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall_assembly/scripts/purge_haplotigs/4purge/purgePAbc/curated.fasta -f 1000 -o /core/labs/Wegrzyn/PumpkinAshGenome/ragtag_scaffolding/scaffolding_only/scaffold_afterpurgePA
