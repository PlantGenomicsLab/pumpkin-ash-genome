#!/bin/bash
#SBATCH --job-name=repeatmodeler_modelPA
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 30
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH --mem=50G
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
echo `date`

module load RepeatModeler/2.01
module load ninja/0.95 

REPDIR=/core/labs/Wegrzyn/PumpkinAshGenome/repeatmodeler/pumpkinash/
cd ${REPDIR}
REPDB=pumpkinash_db
RepeatModeler -pa 30 -database ${REPDB} -LTRStruct
