#!/bin/bash
#SBATCH --job-name=fastpWA
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --mem=50G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
module load fastp/0.23.2
module load parallel/20180122

INDIR=/core/labs/Wegrzyn/PumpkinAshGenome/rnaseq_data_ash/whiteash
REPORTDIR=fastp_reportsWA
mkdir -p $REPORTDIR
TRIMDIR=trimmed_sequencesWA
mkdir -p $TRIMDIR

ACCLIST=$INDIR/accessionlistWA.txt

cat $ACCLIST | parallel -j 4 \
fastp \
	--in1 $INDIR/{}_1.fastq \
	--in2 $INDIR/{}_2.fastq \
	--out1 $TRIMDIR/{}_trim_1.fastq.gz \
	--out2 $TRIMDIR/{}_trim_2.fastq.gz \
	--json $REPORTDIR/{}_fastp.json \
	--html $REPORTDIR/{}_fastp.html

