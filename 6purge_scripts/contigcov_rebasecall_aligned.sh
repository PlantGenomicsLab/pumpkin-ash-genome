#!/bin/bash
#SBATCH --job-name=contigcov_all
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err
echo `hostname`

module load purge_haplotigs

purge_haplotigs contigcov -i /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/purge_haplotigs/3readhist/aligned_rebasecall_contigsrmvPA_sorted.bam.gencov -l 0 -m 27 -h 198 -o coverage_stats_rebasecalledPA.csv
purge_haplotigs contigcov -i /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/purge_haplotigs/3readhist/aligned_rebasecall_contigsrmvWA_sorted.bam.gencov -l 3 -m 45 -h 198 -o coverage_stats_rebaseccalledWA.csv
purge_haplotigs contigcov -i /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/purge_haplotigs/3readhist/aligned_rebasecall_contigsrmvBA_sorted.bam.gencov -l 0 -m 35 -h 198 -o coverage_stats_rebasecalledBA.csv
