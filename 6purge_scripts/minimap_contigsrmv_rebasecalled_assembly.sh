#!/bin/bash
#SBATCH --job-name=minimap_contigrmv_bcassembly
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 32
#SBATCH --mem=125G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
module load minimap2/2.24
module load samtools

minimap2 -ax map-ont -t 32 /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/medaka/medakaPA/consensus.fasta /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/bbmap/all_basecalled_readsPA_uncontaminated.fastq > aligned_rebasecall_contigsrmvPA.sam

minimap2 -ax map-ont -t 32 /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/medaka/medakaWA/consensus.fasta /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/bbmap/all_basecalled_readsWA_uncontaminated.fastq  > aligned_rebasecall_contigsrmvWA.sam

minimap2 -ax map-ont -t 32 /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/medaka/medakaBA/consensus.fasta /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/bbmap/all_basecalled_readsBA_uncontaminated.fastq >  aligned_rebasecall_contigsrmvBA.sam

