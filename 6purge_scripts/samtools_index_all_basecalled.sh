#!/bin/bash
#SBATCH --job-name=samtools
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
module load samtools

samtools index aligned_rebasecall_contigsrmvPA_sorted.bam
samtools index aligned_rebasecall_contigsrmvWA_sorted.bam
samtools index aligned_rebasecall_contigsrmvBA_sorted.bam
