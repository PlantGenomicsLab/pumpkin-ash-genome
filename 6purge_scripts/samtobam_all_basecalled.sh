#!/bin/bash
#SBATCH --job-name=samtobam_bc
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
module load samtools

samtools view -S -b aligned_rebasecall_contigsrmvPA.sam > aligned_rebasecall_contigsrmvPA.bam
samtools view -S -b aligned_rebasecall_contigsrmvWA.sam > aligned_rebasecall_contigsrmvWA.bam    
samtools view -S -b aligned_rebasecall_contigsrmvBA.sam > aligned_rebasecall_contigsrmvBA.bam
