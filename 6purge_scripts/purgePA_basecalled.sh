#!/bin/bash
#SBATCH --job-name=PApurge_basecalled
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo `hostname`
module load purge_haplotigs
module load R

purge_haplotigs purge -b /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/purge_haplotigs/1minimap/aligned_rebasecall_contigsrmvPA_sorted.bam -g /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/medaka/medakaPA/consensus.fasta -c /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/purge_haplotigs/4contigcov/coverage_stats_rebasecalledPA.csv -d -a 60

