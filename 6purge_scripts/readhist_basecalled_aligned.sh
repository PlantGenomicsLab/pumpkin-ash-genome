#!/bin/bash
#SBATCH --job-name=readhist
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=80G
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mail-user=emily.strickland@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%

module load R
module load purge_haplotigs
module load samtools

/isg/shared/apps/purge_haplotigs/1.1.2/bin/purge_haplotigs readhist -b /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/purge_haplotigs/1minimap/aligned_rebasecall_contigsrmvBA_sorted.bam -g /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/medaka/medakaBA/consensus.fasta -t 16

/isg/shared/apps/purge_haplotigs/1.1.2/bin/purge_haplotigs readhist -b /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/purge_haplotigs/1minimap/aligned_rebasecall_contigsrmvWA_sorted.bam -g /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/medaka/medakaWA/consensus.fasta -t 16

/isg/shared/apps/purge_haplotigs/1.1.2/bin/purge_haplotigs readhist -b /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/purge_haplotigs/1minimap/aligned_rebasecall_contigsrmvPA_sorted.bam -g /core/labs/Wegrzyn/PumpkinAshGenome/rebasecall/scripts/medaka/medakaPA/consensus.fasta -t 16
